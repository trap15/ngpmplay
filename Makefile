BASE		 = .
SOURCE		 = $(BASE)/src
# Valid OSDs are:
#    allegro
USED_OSD	 = allegro
OSD		 = $(SOURCE)/osd_$(USED_OSD)
OBJECTS		 = $(SOURCE)/sound.o $(OSD)/osd.o $(SOURCE)/main.o
OUTPUT		 = ngpmplay
DEFS		 = 
OPTIMIZE	 = -O0
#OPTIMIZE	 = -O3
LIBS		 = -lm
LIBDIRS		 = 
INCLUDES	 = -I./
CFLAGS		 = -g -std=c89 -Wall -pedantic $(INCLUDES) $(OPTIMIZE)
LDFLAGS		 = $(LIBDIRS) $(LIBS)
CC		 = gcc
RM		 = rm

ifeq ($(USED_OSD), allegro)
LIBS		+= `allegro-config --static`
CFLAGS		+= `allegro-config --cflags` -DOSD_SUFFIX=END_OF_MAIN -DOSD=1
LDFLAGS		+= -m32
CFLAGS		+= -m32
endif

all: $(OUTPUT)
%.o: %.m
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	$(RM) -f $(OUTPUT) $(OBJECTS)
run:
	./$(OUTPUT)
release: clean
