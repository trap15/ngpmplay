/*****************************************************************************
 *  ngpmplay -- A player for NeoGeo Pocket Music (NGPM) files.               *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2010         trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#include <allegro.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "../osd.h"

#define USE_STEREO 1

#define	PSGSAMPLES		(804 << 1)
#define DACSAMPLES		(134 << 1)

#define RUN_SAMPLES		(1024)

#define SOUND_CALLBACK_TIMING	(30)

static AUDIOSTREAM* psgstream = NULL;
static AUDIOSTREAM* dacstream = NULL;

void allegro_system_sound_callback()
{
	uint16_t* buf;
	int i;
	if(ngpm.pc == 0xFFFFFFFF)
		return;
	system_sound_callback();
	if(psgstream) {
		buf = get_audio_stream_buffer(psgstream);
		if(buf != NULL) {
#if USE_STEREO
			sound_update_lr(psglbuffer, psgrbuffer, PSGSAMPLES << 1);
			if(!mute) {
				for(i = 0; i < PSGSAMPLES; i++) {
					buf[i * 2 + 0] = psglbuffer[i];
					buf[i * 2 + 1] = psgrbuffer[i];
				}
			}else{
				bzero(buf, PSGSAMPLES << 2);
			}
#else
			sound_update(psgbuffer, PSGSAMPLES << 1);
			if(!mute) {
				memcpy(buf, psgbuffer, PSGSAMPLES << 1);
			}else{
				bzero(buf, PSGSAMPLES << 2);
			}
#endif
			if(psgstream)
				free_audio_stream_buffer(psgstream);
		}
	}
	if(dacstream) {
		buf = get_audio_stream_buffer(dacstream);
		if(buf != NULL) {
			dac_update(dacbuffer, DACSAMPLES);
			if(!mute)
				memcpy(buf, dacbuffer, DACSAMPLES);
			else
				bzero(buf, DACSAMPLES);
			if(dacstream)
				free_audio_stream_buffer(dacstream);
		}
	}
}

void osd_update()
{
}

BOOL osd_init()
{
	if(allegro_init() != 0)
		return FALSE;
	if(set_gfx_mode(GFX_AUTODETECT, 128, 128, 0, 0) != 0) {
		printf("Couldn't set screen res.\n");
	}
	install_keyboard(); 
	if(install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL) != 0) {
		printf("Error initializing sound system: %s\n", allegro_error);
		return FALSE;
	}
	if(digi_card == DIGI_NONE) {
		printf("Unable to find a sound driver: %s\n", allegro_error);
		return FALSE;
	}
	return TRUE;
}

BOOL osd_keycheck()
{
	if(keypressed())
		return TRUE;
	return FALSE;
}

void osd_fini()
{
	osd_sound_fini();
}

BOOL osd_sound_reset()
{
	if(!psgstream)
		psgstream = play_audio_stream(PSGSAMPLES, 16, USE_STEREO, SAMPLE_RATE, 255, 128);
	if(!dacstream)
		dacstream = play_audio_stream(DACSAMPLES,  8, 0,        8000, 255, 128);
	install_int_ex(allegro_system_sound_callback, BPS_TO_TIMER(SOUND_CALLBACK_TIMING));
	return TRUE;
}

void osd_sound_unmute()
{
	osd_sound_reset();
}

void osd_sound_mute()
{
	if(psgstream)
		stop_audio_stream(psgstream);
	if(dacstream)
		stop_audio_stream(dacstream);
	psgstream = NULL;
	dacstream = NULL;
}

void osd_sound_fini()
{
	osd_sound_mute();
}
