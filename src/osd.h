/*****************************************************************************
 *  ngpmplay -- A player for NeoGeo Pocket Music (NGPM) files.               *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2010         trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#ifndef _OSD_H_
#define _OSD_H_

#include <stdint.h>

#ifndef BOOL
#define BOOL int
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

typedef struct {
	uint16_t*	trk_name_e;
	uint16_t*	trk_name_j;
	uint16_t*	game_name_e;
	uint16_t*	game_name_j;
	uint16_t*	auth_name_e;
	uint16_t*	auth_name_j;
	char		release[8];
	uint16_t*	ngpmer;
} ngpi_t;

typedef struct {
	float		version;
	uint32_t	ngpi_off;
	uint32_t	ngp_clock;
	uint32_t	pc;
	uint8_t*	data;
	uint32_t	loop_point;
	ngpi_t		ngpi;
} ngpm_t;

#define MAXSAMPLES		(65536)
#define SAMPLE_RATE		(48000)

extern uint16_t psgbuffer[];
extern uint16_t psglbuffer[];
extern uint16_t psgrbuffer[];
extern uint8_t dacbuffer[];
extern uint16_t mixed_buf[];

extern ngpm_t ngpm;
extern BOOL mute;

uint32_t system_sound_callback();
void system_sound_update(int psgsamples, int dacsamples);
void sound_mix_streams(int samples);

BOOL osd_init();
void osd_update();
BOOL osd_keycheck();
void osd_fini();
BOOL osd_sound_reset();
void osd_sound_unmute();
void osd_sound_mute();
void osd_sound_fini();

#endif
