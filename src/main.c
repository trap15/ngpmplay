/*****************************************************************************
 *  ngpmplay -- A player for NeoGeo Pocket Music (NGPM) files.               *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2010         trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#if OSD == 1
#include <allegro.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "osd.h"
#include "sound.h"

uint16_t psglbuffer[MAXSAMPLES];
uint16_t psgrbuffer[MAXSAMPLES];
uint16_t psgbuffer[MAXSAMPLES];
uint8_t dacbuffer[MAXSAMPLES];
uint16_t mixed_buf[MAXSAMPLES];

BOOL mute = FALSE;

ngpm_t ngpm;

uint32_t run_ngpm()
{
	uint32_t smps = 0;
	uint8_t cmd;
	uint8_t* data;
	if(ngpm.pc == 0xFFFFFFFF) {
		return 1;
	}
	cmd = ngpm.data[ngpm.pc++];
	data = ngpm.data + ngpm.pc;
	switch(cmd) {
		case 0x00:
			ngpm.loop_point = ngpm.pc;
			break;
		case 0x01:
			ngpm.pc = 0xFFFFFFFF;
			break;
		case 0x40:
			Write_SoundChipTone(data[0]);
			break;
		case 0x41:
			Write_SoundChipNoise(data[0]);
			break;
		case 0x42:
			dac_write(data[0]);
			break;
		case 0x80:
			smps  = (data[0] << 8);
			smps |= (data[1]);
			break;
	}
	if(cmd >= 0x40)
		ngpm.pc += 1;
	if(cmd >= 0x80)
		ngpm.pc += 1;
	if(cmd >= 0xC0)
		ngpm.pc += 1;
	if(cmd >= 0xE0)
		ngpm.pc += 1;
	return smps;
}

uint32_t system_sound_callback()
{
	uint32_t sampwait = 0;
	while(sampwait == 0)
		sampwait = run_ngpm();
	return sampwait;
}

void system_sound_update(int psgsamples, int dacsamples)
{
	if(!mute) {
		sound_update(psgbuffer, psgsamples << 1);
		dac_update(dacbuffer, dacsamples);
	}
}

void sound_mix_streams(int samples)
{
	int i;
	/* TODO: Mix the DAC */
	for(i = 0; i < samples; i++) {
		mixed_buf[i] = psgbuffer[i];
	}
}

void system_sound_chipreset()
{
	sound_init(SAMPLE_RATE, ngpm.ngp_clock);
	osd_sound_reset();
}

void system_sound_resume()
{
	osd_sound_unmute();
	mute = FALSE;
}

void system_sound_silence()
{
	mute = TRUE;
	osd_sound_mute();
}

BOOL system_sound_init()
{
	system_sound_chipreset();
	return TRUE;
}

void system_sound_shutdown(void)
{
	system_sound_silence();
	osd_sound_fini();
}

void do_update()
{
	osd_update();
}

void print_ngpi(ngpi_t ngpi)
{
	/* TODO: Make a real unicode display :x */
	char tmp[256];
	uint16_t ch;
	int i;
#define PRINT_UNICODE_HACK(str) do { \
	ch = str[0]; \
	tmp[0] = ch & 0xFF; \
	for(i = 1; ch != 0; i++) { \
		ch = str[i]; \
		tmp[i] = ch & 0xFF; \
	} \
} while(0)
	PRINT_UNICODE_HACK(ngpi.trk_name_e);
	printf("		Track Name:	%s\n", tmp);
	PRINT_UNICODE_HACK(ngpi.game_name_e);
	printf("		Game:		%s\n", tmp);
	PRINT_UNICODE_HACK(ngpi.auth_name_e);
	printf("		Author:		%s\n", tmp);
	printf("		Release Date:	%c%c%c%c/%c%c/%c%c\n", ngpi.release[0], \
	       ngpi.release[1], ngpi.release[2], ngpi.release[3], \
	       ngpi.release[4], ngpi.release[5], ngpi.release[6], \
	       ngpi.release[7]);
	PRINT_UNICODE_HACK(ngpi.ngpmer);
	printf("		Converted By:	%s\n", tmp);
}

void read_unicode_string(FILE* fp, uint16_t** string)
{
	uint32_t off;
	uint16_t ch = 1;
	int i = 1;
	off = ftell(fp);
	fread(&ch, 2, 1, fp);
	for(; ch != 0; i++) {
		fread(&ch, 2, 1, fp);
	}
	*string = calloc(2, i);
	fseek(fp, off, SEEK_SET);
	i = 0;
	fread(&ch, 2, 1, fp);
	ch = ntohs(ch);
	(*string)[i] = ch;
	for(i = 1; ch != 0; i++) {
		fread(&ch, 2, 1, fp);
		ch = ntohs(ch);
		(*string)[i] = ch;
	}
}

BOOL validate_file(FILE* fp)
{
	char ident[4];
	uint32_t ver;
	uint32_t size;
	uint32_t tmp;
	
	fseek(fp, 0x00, SEEK_SET);
	fread(ident, 4, 1, fp);
	if((ident[0] != 'N') || (ident[1] != 'G') || (ident[2] != 'P') || (ident[3] != 'M'))
		return FALSE;
	
	fseek(fp, 0x0C, SEEK_SET);
	fread(&ver, 4, 1, fp);
	ver = ntohl(ver);
	ngpm.version = ver >> 16;
	if((ver & 0xFFFF) != 0)
	ngpm.version += 1.0 / (65536.0 / (float)(ver & 0xFFFF));
	
	fseek(fp, 0x10, SEEK_SET);
	fread(&(ngpm.ngpi_off), 4, 1, fp);
	ngpm.ngpi_off = ntohl(ngpm.ngpi_off);
	
	fseek(fp, 0x14, SEEK_SET);
	fread(&(ngpm.ngp_clock), 4, 1, fp);
	ngpm.ngp_clock = ntohl(ngpm.ngp_clock);
	ngpm.pc = 0;
	
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0x04, SEEK_SET);
	fread(&tmp, 4, 1, fp);
	tmp = ntohl(tmp);
	size -= tmp;
	ngpm.data = calloc(size, 1);
	if(!ngpm.data)
		return FALSE;
	fseek(fp, tmp, SEEK_SET);
	fread(ngpm.data, size, 1, fp);
	fseek(fp, 0, SEEK_SET);
	if(ngpm.ngpi_off) {
		fseek(fp, ngpm.ngpi_off, SEEK_SET);
		fread(ident, 4, 1, fp);
		if((ident[0] != 'N') || (ident[1] != 'G') || (ident[2] != 'P') || (ident[3] != 'I')) {
			ngpm.ngpi_off = 0;
			return TRUE;
		}
#define READ_NGPI_CHUNK(off, part) do { \
		fseek(fp, ngpm.ngpi_off + off, SEEK_SET); \
		fread(&tmp, 4, 1, fp); \
		tmp = ntohl(tmp); \
		fseek(fp, ngpm.ngpi_off + tmp, SEEK_SET); \
		read_unicode_string(fp, &(ngpm.ngpi.part)); \
} while(0)
		READ_NGPI_CHUNK(0x08, trk_name_e);
		READ_NGPI_CHUNK(0x0C, trk_name_j);
		READ_NGPI_CHUNK(0x10, game_name_e);
		READ_NGPI_CHUNK(0x14, game_name_j);
		READ_NGPI_CHUNK(0x18, auth_name_e);
		READ_NGPI_CHUNK(0x1C, auth_name_j);
		fseek(fp, ngpm.ngpi_off + 0x20, SEEK_SET);
		fread(ngpm.ngpi.release, 8, 1, fp);
		READ_NGPI_CHUNK(0x28, ngpmer);
	}
	return TRUE;
}

int main(int argc, char *argv[])
{
	FILE* fp;
	int running = 1;
	if(!osd_init())
		return 4;
	fp = fopen(argv[1], "rb");
	if(fp == NULL)
		return 1;
	if(!validate_file(fp))
		return 2;
	fclose(fp);
	printf("Song data:\n");
	printf("	NGPM version %1.2f\n", ngpm.version);
	printf("	NGP Audio Clock: %d\n", ngpm.ngp_clock);
	printf("	NGPI data:	 %s\n", ngpm.ngpi_off ? "YES!" : "NO");
	if(ngpm.ngpi_off) {
		print_ngpi(ngpm.ngpi);
	}
	if(!system_sound_init())
		return 3;
	while(running) {
		if(osd_keycheck())
			running = 0;
		if(ngpm.pc == 0xFFFFFFFF)
			running = 0;
		do_update();
		sleep(0.0001);
	}
	osd_fini();
	return 0;
}

#if OSD == 1
END_OF_MAIN()
#endif
